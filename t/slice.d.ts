/**
 * 用于Koa的一些扩展定义
 * Copyright(C) 2017-2018 liumurong
 */
import * as Koa from "koa";
import { Readable } from "stream";
import { SliceOptions, MetadataOptions } from "../receive/models";
import * as Application from "koa";
// 扩展KOA上下文
declare module 'koa'
{
    interface Context {
        slice: {
            stream: Readable,       // 流
            metadata: MetadataOptions,
            [name: string]: any     // 其他属性
        }
    }
}
// 接收数据
export declare function sliceResume(app: Application, opts?: SliceOptions): Application.Middleware;
// 后台按照规则获取数据
export declare function sliceArchiver(ctx: Application.Context, next: () => Promise<any>): Promise<any>;
// 直接在流中读取数据
export declare function sliceArchiverStream(hashCode: string): Promise<Readable>;
// 设置slice选项
export declare function setSliceOptions(opts: SliceOptions): void;