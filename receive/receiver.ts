/**
 * 数据接收者
 * Copyright(C) 2018 liumurong
 */

import { Readable } from 'stream';
import { FileSlice, MetadataOptions } from './models';
import { MerkleTree } from 'merkletreets';


/**
 * 文件接收者
 */
export class Receiver {
    protected target: string;   // 文件切片存储路径
    // 默克尔树集合
    protected merkleTrees: Map<string, MerkleTree>;
    /**
     * 文件接收者
     * @param target 文件存储路径 
     */
    constructor(target: string) {
        this.target = target;
        this.merkleTrees = new Map<string, MerkleTree>();
    }
    /**
     * 保存切片
     * @param slice 文件切片
     */
    async saveSlice(slice: FileSlice) {
    }

    /**
     * 获取默克尔树中节点的状态
     * @param hash 文件HASH。采用sha256算法
     * @param depth 默克尔树节点深度，根节点为0
     * @param index 节点在当前深度的索引
     */
    async getNodeHash(hash: string, depth: string, index: string): Promise<string> {
        return null;
    }
    /**
     * 获取文件元数据
     * @param hash 文件HASH，采用sha256算法
     */
    async getMetadata(hash: string): Promise<MetadataOptions> {
        return null;
    }

    /**
     * 获取已经上传的文件的片数
     */
    async getUploadedCount(hash: string): Promise<number> {
        return null;
    }

    /**
     * 获取文件流
     * @param hash 文件MD5
     */
    getStream(hash: string): Readable {
        return null;
    }

    /**
    * 获取不完整的资源文件
    * Hash不匹配且上传日期为一周之前则认为是垃圾资源
    * @param pagenumber 页码
    * @param pagecount 页数
    */
    async getUnIntegralResourceFiles(pagenumber: number, pagecount: number): Promise<MetadataOptions[]> {
        return null;
    }
    /**
     * 删除用户上传的资源文件
     * @param hash HASH
     */
    async removeResourceFile(hash: string) {

    }
    /**
     * 删除全部不完整的资源文件
     */
    async removeUnIntegralResourceFiles() {

    }


}
