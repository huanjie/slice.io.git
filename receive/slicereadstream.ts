/**
 * 切片流读取对象
 * Copyright(C) 2018 liumurong
 */

import { Readable } from 'stream';
import { MetadataOptions } from './models';


/**
 * 切片读取流
 */
export class SliceReadStream extends Readable {
    protected sliceIndex: number;
    protected hash: string
    protected source: string
    protected metadata: MetadataOptions
    constructor(hash: string, source: string) {
        super()
        this.hash = hash;
        this.sliceIndex = 0;
        this.source = source;
    }
}

