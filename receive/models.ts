/**
 * 系统通用结构体定义
 * Copyright(C) 2018 liumurong
 */
/**
 * 基础配置参数
 */
export interface SliceOptions {
    /**
     * Mongodb链接字符串名称
     */
    mongoconnstring?: string,
    /**
     * 本地
     */
    localstoredirectory?: string,
    /**
     * 文件MD5字段名称
     */
    hashfieldname?: string
}

/**
 * 文件切片
 */
export interface FileSlice {
    hash: string
    name: string
    index: number
    slicecount: number
    size: number
    chunksize: number
    data: Buffer
}

/**
 * 文件元数据
 */
export interface MetadataOptions {
    hash: string
    name: string
    index: number
    slicecount: number
    size: number
    root: string
}

