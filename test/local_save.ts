/**
 * 硬盘版本测试
 * Copyright(C) 2018 liumurong
 */

import { ReceiverHelper } from "../receive/receiverhelper";
import * as fs from "fs";


ReceiverHelper.instance.setOptions({
    localstoredirectory: "E:\\slice"
});
let chunksize = 2 * 1024 * 1024;    // 2M
let filepath = "E:\\slice\\code.rar";
let index = 0;

/**
 * 保存
 */
async function save() {
    let instream = fs.createReadStream(filepath);
    return new Promise((resolve, reject) => {
        instream.on("data", (buf) => {
            (function () {
                ReceiverHelper.instance.receiver.saveSlice({
                    hash: "4bf8932bd15cca0398ff6bf51c76e5f8b606d31acd4b6551f92bdf6098ce290c",
                    name: "t1",
                    index: index,
                    slicecount: index + 1,
                    size: 23,
                    chunksize: chunksize,
                    data: buf
                })
                index++;
            })()
            // 可能会导致顺序错乱
        })
        instream.on("end", () => {
            resolve();
        })
    })
}
console.log("start");
save().then(() => {
    console.log("end");
});









