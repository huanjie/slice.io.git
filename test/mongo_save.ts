
/**
 * 本地数据库测试
 * @author renyu
 */

import { ReceiverHelper } from '../receive/receiverhelper';
import * as fs from 'fs';

ReceiverHelper.instance.setOptions({
	mongoconnstring: 'mongodb://admin:admin@localhost:27017/mapserver'
});

const filepath = 'E:\\slice\\code.rar';
let index = 0;
let chunks: Buffer[] = [];
async function save() {
	let instream = fs.createReadStream(filepath);
	return new Promise((resolve, reject) => {
		instream.on('data', buf => {
			chunks.push(buf);
			// (function () {
			// 	ReceiverHelper.instance.receiver.saveSlice({
			// 		hash: 't2',
			// 		name: 't2',
			// 		index,
			// 		slicecount: index + 1,
			// 		size: 23,
			// 		chunksize: 2 * 1024 * 1024,
			// 		data: buf
			// 	});
			// 	index++;
			// })();
			// 可能会导致顺序错乱
		});
		instream.on('end', () => {
			resolve();
		});
		instream.on('close', () => {
			resolve();
		});
		instream.on('error', (err) => {
			console.log(err);
		});
	});
}
main();
async function main() {
	console.log('start');
	await save();
	for (let i = 0; i < chunks.length; i++) {
		const element = chunks[i];
		await ReceiverHelper.instance.receiver.saveSlice({
			hash: '4bf8932bd15cca0398ff6bf51c76e5f8b606d31acd4b6551f92bdf6098ce290c',
			name: 't5',
			index: i,
			slicecount: i + 1,
			size: 23,
			chunksize: 2 * 1024 * 1024,
			data: element
		});
	}
	console.log('end');
}